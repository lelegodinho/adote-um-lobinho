console.log("teste")

const urlApi = `https://lobinhos.herokuapp.com/wolves/`

function createLobo1(urlImg1, descrLobo1, nomeLobo1, idadeLobo1){
    let quadroAzulImpar = document.querySelector(".quadroAzulImpar")
    let loboImage = document.createElement("img")
    loboImage.setAttribute('src', urlImg1)
    quadroAzulImpar.append(loboImage)
    console.log(urlImg1)

    let descrLoboImpar = document.querySelector(".descrLoboImpar")
    descrLoboImpar.innerHTML = descrLobo1

    let nomeLoboImpar = document.querySelector(".nomeLoboImpar")
    nomeLoboImpar.innerHTML = nomeLobo1

    let idadeLoboImpar = document.querySelector(".idadeLoboImpar")
    idadeLoboImpar.innerHTML = ('Idade: ' + idadeLobo1 +' anos')
}


function createLobo2(urlImg2, descrLobo2, nomeLobo2, idadeLobo2){
    let quadroAzulPar = document.querySelector(".quadroAzulPar")
    let loboImage = document.createElement("img")
    loboImage.setAttribute('src', urlImg2)
    quadroAzulPar.append(loboImage)
    console.log(urlImg2)

    let descrLoboPar = document.querySelector(".descrLoboPar")
    descrLoboPar.innerHTML = descrLobo2

    let nomeLoboPar = document.querySelector(".nomeLoboPar")
    nomeLoboPar.innerHTML = nomeLobo2

    let idadeLoboPar = document.querySelector(".idadeLoboPar")
    idadeLoboPar.innerHTML = ('Idade: ' + idadeLobo2 +' anos')
}





let fetchConfig = {
    method: "GET"
}

fetch(urlApi, fetchConfig)
    .then(resposta => resposta.json() 
        .then(resp => {

            let max = resp.length
            console.log(max)
            let numLobo1 = Math.floor(Math.random() * max )
            let numLobo2 = Math.floor(Math.random() * max )

            var objLobo1 = resp[numLobo1]
            const nomeLobo1 = objLobo1.name
            const urlImg1 = objLobo1.image_url
            const descrLobo1 = objLobo1.description
            const idadeLobo1 = objLobo1.age
            createLobo1(urlImg1, descrLobo1, nomeLobo1, idadeLobo1)

            var objLobo2 = resp[numLobo2]
            const nomeLobo2 = objLobo2.name
            const urlImg2 = objLobo2.image_url
            const descrLobo2 = objLobo2.description
            const idadeLobo2 = objLobo2.age
            createLobo2(urlImg2, descrLobo2, nomeLobo2, idadeLobo2)
            
        })
        .catch( error => { console.log ("catch")})
        )
    .catch( error => { console.log ("catch")})



