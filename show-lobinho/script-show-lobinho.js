const urlParams = new URLSearchParams(window.location.search);
const idLobo = urlParams.get('lobo');
const url = `https://lobinhos.herokuapp.com/wolves/${idLobo}`
const containerMensage = document.querySelector(".containerLobo")

let fetchConfig = {
    method: "GET"
}

function createInfos(name, description, image_url, adopted){
    let infoLobos = document.createElement("div")
    infoLobos.classList.add("infoLobos")
    infoLobos.setAttribute("adopted", adopted)
  
    let nameLobo = document.createElement("span")
    nameLobo.classList.add("nameLobo")
    nameLobo.innerText = `${name}`

    let descriptionLobo = document.createElement("span")
    descriptionLobo.classList.add("descriptionLobo")
    descriptionLobo.innerText = `${description}`

    let imgLobo = document.createElement("img")
    imgLobo.classList.add("img")
    imgLobo.setAttribute('src', image_url);

    infoLobos.appendChild(descriptionLobo)
    infoLobos.appendChild(imgLobo)
    
    containerMensage.appendChild(nameLobo)
    containerMensage.append(infoLobos)
}

function getApi(url){
    fetch(url, fetchConfig)
        .then(response => response.json()
        .then(lobo => {
            createInfos(lobo.name, lobo.description, lobo.image_url, lobo.adopted)
            })
            .catch(error => {console.log(error)})
        )
        .catch(error => {console.log(error)})
}


function adotar (btn){
    window.location.href = `adotar-lobinho.html?lobo=${idLobo}`
}


function excluir(btn){
    console.log(btn)
}

getApi(url)